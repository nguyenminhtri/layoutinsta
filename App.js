import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  ScrollView,
} from "react-native";
import data from "./data.json";
import Content from "./component/layout/Content";
import Header from "./component/layout/Header";
import TabBar from "./component/layout/TabBar";
import ListFollowed from "./component/layout/ListFollowed";
export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <Header />
      <ListFollowed data={data} />
      <FlatList
        data={data}
        renderItem={({ item }) => <Content item={item} />}
        keyExtractor={(item) => item.id}
      />
      <TabBar />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 30,
  },
});
