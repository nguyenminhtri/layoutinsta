import React from "react";
import { View, Image, FlatList } from "react-native";

export default function ListFollowed({ data }) {
  return (
    <View
      style={{
        paddingBottom: 10,
        borderBottomWidth: 0.2,
        borderBottomColor: "gray",
      }}
    >
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={data}
        renderItem={({ item }) => (
          <View>
            <Image
              source={{ uri: item.avatar }}
              style={{
                width: 50,
                height: 50,
                borderRadius: 50,
                marginHorizontal: 5,
              }}
            />
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}
