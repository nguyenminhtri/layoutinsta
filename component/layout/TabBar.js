import React from "react";
import { View } from "react-native";
import FontAwesome from "@expo/vector-icons/FontAwesome";
export default function TabBar() {
  return (
    <View
      style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-around",
      }}
    >
      <FontAwesome style={{ fontSize: 25 }} name="home" />
      <FontAwesome style={{ fontSize: 25 }} name="search" />
      <FontAwesome style={{ fontSize: 25 }} name="address-book" />
      <FontAwesome style={{ fontSize: 25 }} name="share" />
      <FontAwesome style={{ fontSize: 25 }} name="user-circle" />
    </View>
  );
}
