import React from "react";
import { Text, View, Image } from "react-native";
import FontAwesome from "@expo/vector-icons/FontAwesome";
export default function Content({ item }) {
  return (
    <View
      style={{
        paddingVertical: 5,
        borderColor: "gray",
        borderWidth: 0.5,
      }}
    >
      <View
        style={{
          borderBottomColor: "gray",
          marginBottom: 10,
        }}
      >
        <View
          style={{
            marginHorizontal: 20,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 5,
            }}
          >
            <Image
              style={{
                width: 40,
                height: 40,
                borderRadius: 50,
                marginRight: 10,
              }}
              source={{ uri: item.avatar }}
            />

            <Text style={{ fontWeight: 500 }}>{item.name}</Text>
          </View>
          <FontAwesome name="ellipsis-h" style={{ fontSize: 20 }} />
        </View>

        <Image
          style={{ width: 400, height: 300 }}
          source={{ uri: item.image }}
        />
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginHorizontal: 20,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <FontAwesome name="heart-o" style={{ fontSize: 20 }} />
            <FontAwesome
              name="comment-o"
              style={{ fontSize: 20, marginHorizontal: 10 }}
            />
            <FontAwesome name="location-arrow" style={{ fontSize: 20 }} />
          </View>
          <FontAwesome name="bookmark-o" style={{ fontSize: 20 }} />
        </View>
        <View style={{ paddingHorizontal: 20 }}>
          <Text>
            Mô tả:
            {item.desc.length > 60
              ? item.desc.substring(0, 60) + "..."
              : item.desc}
          </Text>
        </View>
      </View>
    </View>
  );
}
