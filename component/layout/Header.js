import React from "react";
import { TextInput, Text, View, StyleSheet, Image } from "react-native";
import FontAwesome from "@expo/vector-icons/FontAwesome";
export default function Header() {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginHorizontal: 45,
      }}
    >
      <Image
        style={{ width: 120, height: 80 }}
        resizeMode="contain"
        source={{
          uri: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Instagram_logo.svg/840px-Instagram_logo.svg.png",
        }}
      />
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <FontAwesome name="heart-o" style={{ fontSize: 20, marginRight: 30 }} />
        <FontAwesome name="comment-o" style={{ fontSize: 20 }} />
      </View>
    </View>
  );
}
